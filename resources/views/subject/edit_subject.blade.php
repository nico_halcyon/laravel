@extends('layouts.app')
@section('title')
    Edit Subject
@endsection
@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">
                    EDIT SUBJECT 
                    <a href="{{url('subject')}}">
                        <label class="back-btn btn btn-default btn-sm"> <- Back</label>
                    </a>
                </div>

                <div class="panel-body">

                    @if (count($errors) > 0)
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif

                    <form method="post" action="{{url('subject/update')}}">
                        {{csrf_field()}}
                        <input type="hidden" name="id" value="{{$subject->id}}">
                        <input class="form-control input" type="text" name="subject" value="{{$subject->subject_name}}" placeholder="Enter Subject">
                        <input class="form-control input" type="text" name="unit" value="{{$subject->unit}}" placeholder="Enter Unit">
                        <button class="form-control btn btn-success input">Save</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection


