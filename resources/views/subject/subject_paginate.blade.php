<table class="table table-bordered table-stiped">
    <thead>
        <tr>
            <th>Subject</th>
            <th>Unit</th>
            <!-- <th>Status</th> -->
            <th>Action</th>
        </tr>
    </thead>
    <tbody>
    @foreach($subjects as $subject)
        <tr>
            <td>{{$subject->subject_name}}</td>
            <td>{{$subject->unit}}</td>
            <!-- <td>
                @if($subject->status == 0)
                    Active
                @else
                    Inactive
                @endif
            </td> -->
            <td>
                <a href="{{url('subject/edit')}}/{{$subject->id}}">
                    <span class="action_subject glyphicon glyphicon-pencil"></span>
                </a>
                <a href="{{url('subject/delete')}}/{{$subject->id}}">
                    <span class="action_subject glyphicon glyphicon-remove"></span>
                </a>
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
{{$subjects->links()}}