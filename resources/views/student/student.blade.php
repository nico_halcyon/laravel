@extends('layouts.app')
@section('title')
    Students
@endsection
@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">
                    STUDENTS TABLE 
                    <a href="{{url('student/add')}}">
                        <label class="add-btn btn btn-primary btn-sm">Add Student</label>
                    </a>
                </div>

                <div class="panel-body">

                    @if(Session::has('message'))
                        <div class="form-control notification">{{Session::get('message')}}</div>
                    @endif

                    <table class="table table-bordered table-stiped">
                        <thead>
                            <tr>
                                <th>Firstname</th>
                                <th>Lastname</th>
                                <th>Email</th>
                                <th>Age</th>
                                <th>Subjects</th>
                                <!-- <th>Status</th> -->
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                        @foreach($students as $student)
                            <tr>
                                <td>{{$student->firstname}}</td>
                                <td>{{$student->lastname}}</td>
                                <td>{{$student->email}}</td>
                                <td>{{$student->age}}</td>
                                <td>
                                    <?php
                                        $subjects = json_decode($student->studentsubject['subject_id']);
                                    ?>
                                    <select class="form-control">
                                    @if($subjects != null)
                                        @foreach($subjects as $subject_id)
                                            <?php
                                                $subject = DB::table('subjects')->where('id',$subject_id)->first();
                                            ?>
                                            @if($subject != null || $subject != "")
                                                <option>{{$subject->subject_name}}</option>
                                            @endif
                                        @endforeach
                                    @endif
                                    </select>


                                </td>
                                <!-- <td>
                                    @if($student->status == 0)
                                        Active
                                    @else
                                        Inactive
                                    @endif
                                </td> -->
                                <td>
                                    <a href="{{url('student/edit')}}/{{$student->id}}">
                                        <span class="action_subject glyphicon glyphicon-pencil"></span>
                                    </a>
                                    <a href="{{url('student/delete')}}/{{$student->id}}">
                                        <span class="action_subject glyphicon glyphicon-remove"></span>
                                    </a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
