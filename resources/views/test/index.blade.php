@extends('layouts.app')
@section('title')
    Students
@endsection
@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">
                    XAMPLE
                </div>

                <div class="panel-body">
                
	                <div class="content">
	                	@foreach($subjects as $subject)
	                		<p>{{$subject->subject_name}}</p>
	                	@endforeach

	                	{{$subjects->links()}}

	                </div>

                    {{-- <button class="btn btn-default ajax">Button</button> --}}
                </div>
            </div>
        </div>
    </div>
</div>

<script>
$(document).ready(function(){

	$(document).on('click','.pagination a',function(e){
		e.preventDefault();
		var page = $(this).attr('href').split('page=')[1];

		getSubjects(page);
	});

	function getSubjects(page){
		$.ajax({
			url: "{{url('test/products')}}",
			type: "GET",
			data: {page:page},
			success:function(data){
				$('.content').html(data);
			}
		});
	}


	$('.ajax').click(function(){
		$.ajax({
			url: "{{url('test/grade')}}",
			type: "GET",
			data: {data:"nico"},
			success:function(data){
				alert('success');
			},
			error:function(){
				alert('error');
			}
		});
	});

});

</script>

@endsection
