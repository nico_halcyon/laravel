<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Subject;
use DB;
use Session;

class SubjectController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $subjects = Subject::paginate(3);
        return view('subject/subject')->withSubjects($subjects);
    }

    public function paginate(){
        $subjects = Subject::paginate(3);
        return view('subject/subject_paginate')->withSubjects($subjects)->render();
    }

    public function add(){
        return view('subject/add_subject');
    }

    public function save(Request $request){

        $this->validate($request,[
            'subject' => 'required',
            'unit'    => 'required|numeric'
        ]);

        $subject_name  = $request->input('subject');
        $unit          = $request->input('unit');


        $subject               = new Subject;
        $subject->subject_name = $subject_name;
        $subject->unit         = $unit;
        $subject->status       = 0;
        $subject->save();

        Session::flash('message','New subject added');

        return redirect('subject');

    }

    public function edit($id){

        $subject = Subject::find($id);
        return view('subject/edit_subject')->withSubject($subject);

    }

    public function update(Request $request){

        $this->validate($request,[
            'subject' => 'required',
            'unit'    => 'required|numeric'
        ]);

        $id            = $request->input('id');
        $subject_name  = $request->input('subject');
        $unit          = $request->input('unit');


        $subject               = Subject::find($id);
        $subject->subject_name = $subject_name;
        $subject->unit         = $unit;
        $subject->save();

        Session::flash('message','Subject Updated');

        return redirect('subject');

    }

    public function delete($id){

        DB::table('subjects')->where('id',$id)->delete();
        
        Session::flash('message','Subject deleted');

        return redirect('subject');

    }

}
