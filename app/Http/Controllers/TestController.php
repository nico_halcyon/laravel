<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use App\Subject;



class TestController extends Controller
{
    public function getIndex(){

    	$subjects = Subject::paginate(2);
    	return view('test/index')->withSubjects($subjects);
    	// return view('test/index');
    }


    public function getProducts(){
    	$subjects = Subject::paginate(2);
    	return view('test/products')->withSubjects($subjects)->render();
    }


    public function getGrade(){
    	return input::get('data');
    	return "Grade";
    }
}
