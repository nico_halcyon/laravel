<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Student;
use App\Subject;
use App\Student_subject;
use DB;
use Illuminate\Support\Facades\Input;
use Session;

class StudentController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */





    public function index()
    {

        $students = Student::with('studentsubject')->get();

        return view('student/student')->withStudents($students);
    }

    public function add(){

        $subject = Subject::all();
        return view('student/add_student')->withSubjects($subject);

    }

    public function save(Request $request){

        $this->validate($request,[
            'firstname'   => 'required',
            'lastname'    => 'required',
            'email'       => 'required|email',
            'age'         => 'required|numeric',
            'subject'     => 'required'
        ]);

        $subject       = json_encode($request->input('subject'));
        $firstname     = $request->input('firstname');
        $lastname      = $request->input('lastname');
        $email         = $request->input('email');
        $age           = $request->input('age');


        $student = new Student;

        $student->firstname = $firstname;
        $student->lastname = $lastname;
        $student->email     = $email;
        $student->age       = $age;
        $student->status    = 0;
        $student->save();


        $student_subjects = new Student_subject;
        $student_subjects->student_id = $student->id;
        $student_subjects->subject_id = $subject;
        $student_subjects->save();

        Session::flash('message','New student added');

        return redirect('student');

    }


    public function edit($id){

        $student        = Student::find($id);
        $subject        = Subject::all();
        $studentsubject = Student_subject::where('student_id',$id)->first();


        $data = [];
        $data['student']        = $student;
        $data['subjects']       = $subject;
        $data['studentsubject'] = json_decode($studentsubject->subject_id);

        return view('student/edit_student')->withData($data);

    }



    public function update(Request $request){

        $this->validate($request,[
            'firstname'   => 'required',
            'lastname'    => 'required',
            'email'       => 'required|email',
            'age'         => 'required|numeric',
            'subject'     => 'required'
        ]);

        $id            = $request->input('id');
        $subject       = json_encode($request->input('subject'));
        $firstname     = $request->input('firstname');
        $lastname      = $request->input('lastname');
        $email         = $request->input('email');
        $age           = $request->input('age');


        $student            = Student::find($id);
        $student->firstname = $firstname;
        $student->lastname  = $lastname;
        $student->email     = $email;
        $student->age       = $age;
        $student->save();


        DB::table('student_subjects')
            ->where('student_id',$id)
            ->update([
                'subject_id' => $subject
            ]);

        Session::flash('message','Student updated');

        return redirect('student');

    }



    public function delete($id){

        DB::table('students')->where('id',$id)->delete();
        
        Session::flash('message','Student deleted');

        return redirect('student');
        
    }


}
