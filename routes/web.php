<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home',       'HomeController@index');


// StudentController
Route::get('/student',             'StudentController@index');
Route::get('/student/add',         'StudentController@add');
Route::post('/student/save',       'StudentController@save');
Route::get('/student/delete/{id}', 'StudentController@delete');
Route::get('/student/edit/{id}',   'StudentController@edit');
Route::post('/student/update',     'StudentController@update');



// SubjectController
Route::get('/subject',             'SubjectController@index');
Route::get('/subject/add',         'SubjectController@add');
Route::post('/subject/save',       'SubjectController@save');
Route::get('/subject/delete/{id}', 'SubjectController@delete');
Route::get('/subject/edit/{id}',   'SubjectController@edit');
Route::post('/subject/update',     'SubjectController@update');
Route::get('/subject/paginate',    'SubjectController@paginate');


AdvancedRoute::controller('test',  'TestController');

